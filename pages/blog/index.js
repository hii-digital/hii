import Link from 'next/link'
import React from 'react'
import Layout from '../../components/Layout/'
import StoryblokService from '../../utils/storyblok-service'

export default class extends React.Component {

     static async getInitialProps({ query }) {
          StoryblokService.setQuery(query)

          let [blogPosts, settings, footer_settings] = await Promise.all([
               StoryblokService.get('cdn/stories', {
                    starts_with: `blog`
               }),
               StoryblokService.get(`cdn/stories/en/settings`),
               StoryblokService.get(`cdn/stories/en/footer-settings`)
          ])

          return {
               blogPosts,
               settings,
               footer_settings
          }
     }

     render() {
          const settingsContent = this.props.settings.data.story
          const footerSettings = this.props.footer_settings.data.story
          const { blogPosts } = this.props

          return (
               <Layout settings={settingsContent} footerSettings={footerSettings}>
                    {blogPosts.data.stories.map((blogPost, index) => {
                         const { published_at, content: { name, intro } } = blogPost
                         return (
                              <div key={index} className="blog__overview">
                                   <h2>
                                        <Link href={'/' + blogPost.full_slug}>
                                             <a className="blog__detail-link">
                                                  {name}
                                             </a>
                                        </Link>
                                   </h2>
                                   <small>
                                        {published_at}
                                   </small>
                                   <p>
                                        {intro}
                                   </p>
                              </div>
                         )
                    })}
               </Layout>
          )
     }
}