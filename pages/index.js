import React, { Component } from 'react'
import Layout from '../components/Layout/index'
import Page from '../components/Page'
import StoryblokService from '../utils/storyblok-service'

class Home extends Component {
	constructor(props) {
		super(props)
		this.state = {
			pageContent: props.page.data.story.content,
		}
	}

	static async getInitialProps({ query }) {
		StoryblokService.setQuery(query)
		// console.log('aa', query);
		// let slug = 'home'
		let [page, settings] = await Promise.all([
			StoryblokService.get(`cdn/stories/home`),
			StoryblokService.get('cdn/stories/en/settings')
		])

		return {
			page,
			settings
		}
	}

	componentDidMount() {
		StoryblokService.initEditor(this)
	}

	render() {
		const name = 'Hii Digital';
		const siteTitle = 'Digital Cannabis Agency';
		const description = 'Taking a blank canvas approach.';
		const settingsContent = this.props.settings.data.story
		const bodyOfPage = this.state.pageContent.body
		// console.log('aa', this.props);
		return (
			<Layout settings={settingsContent}>
				<Page body={bodyOfPage} />
			</Layout>
		)
	}
}

export default Home