import App from "next/app"
import React, { Fragment } from "react"
import '../styles/main.scss'

class MyApp extends App {
     static async getInitialProps({ Component, ctx }) {
          let pageProps = {}
          if (Component.getInitialProps) {
               pageProps = await Component.getInitialProps(ctx)
          }
          return {
               pageProps,
          }
     }
     render() {
          const { Component, pageProps } = this.props
          return (
               <Fragment>
                    <Component {...pageProps} />
               </Fragment>
          )
     }
}

export default MyApp