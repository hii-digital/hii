import Document, { Head, Main, NextScript } from 'next/document';
import React from 'react';
import StoryblokService from '../utils/storyblok-service';

export default class MyDocument extends Document {
     render() {
          return (
               <html>
                    <Head>
                         <script dangerouslySetInnerHTML={{ __html: `var StoryblokCacheVersion = '${StoryblokService.getCacheVersion()}';` }}></script>
                         <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,700;1,500&display=swap" rel="stylesheet"></link>
                    </Head>
                    <body>
                         <Main />
                         <NextScript />
                    </body>
               </html>
          )
     }
}