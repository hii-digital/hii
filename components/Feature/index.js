// import { CSSRulePlugin } from "gsap/dist/CSSRulePlugin";
import { gsap } from "gsap/dist/gsap";
// import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { useEffect, useRef } from 'react';
import SbEditable from 'storyblok-react';
import Component from '../index';

const Feature = ({ blok }) => {

     let feature = useRef(null);
     let tl = gsap.timeline({ defaults: { ease: 'easeIn' } });
     const teasers = useRef(null)

     useEffect(() => {
          // gsap.registerPlugin(CSSRulePlugin, ScrollTrigger);
          gsap.to(feature, 0, {
               css: { visibility: 'visible', }
          })
          console.log(teasers);
          // Feature Animation
          // tl.from(feature, {
          //      scrollTrigger: {
          //           markers: true,
          //           trigger: feature,
          //           // start: 'top center',
          //           // end: 'bottom 80%',
          //           // toggleActions: 'restart pause reverse pause',
          //      },
          //      y: 400,
          //      duration: 3
          // }, 0)
     })

     return (
          <SbEditable content={blok}>
               <div className={`feature`} ref={el => feature = el} >
                    {blok.features.map((blok, index) =>
                         <Component key={index} blok={blok} />
                    )}
               </div>
          </SbEditable>
     )
}

export default Feature