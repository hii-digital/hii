import SbEditable from 'storyblok-react'
import Component from '../index'

const Page = ({ body }) => (
     <SbEditable content={body}>
          <>
               {body && body.map((blok) =>
                    <Component blok={blok} key={blok._uid} />
               )}
          </>
     </SbEditable>
)

export default Page