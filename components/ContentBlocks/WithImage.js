import marked from 'marked';
import SbEditable from 'storyblok-react';
import cbIcon from '../../assets/images/hii_rise_icon.svg';

const ContentBlockWithImage = ({ blok }) => {
     const body = (content) => {
          let rawMarkup = marked(content)
          return { __html: rawMarkup }
     }

     return (
          <SbEditable content={blok}>
               <div className="content-block content-block--with-image">
                    <div className={`container container--large`}>
                         <div className={`row row--align-center ${blok.modifier ? 'row--' + blok.modifier : ''}`}>
                              <div className={`col col--6`}>
                                   <div className={`content-block__content`}>
                                        <div className={`content-block__content-title-emblem`}>
                                             <img src={cbIcon} alt={''} />
                                        </div>
                                        <div className={`content-block__content-title`}>
                                             <h2>{blok.title}</h2>
                                        </div>
                                        {blok.content && (
                                             <div className={`content-block__content-body`} dangerouslySetInnerHTML={body(blok.content)}></div>
                                        )}
                                   </div>
                              </div>
                              <div className={`col col--6`}>
                                   <div className={`content-block__image`}>
                                        {blok.image && (
                                             <img src={blok.image.filename} alt={blok.image.alt} />
                                        )}
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </SbEditable>
     )
}

export default ContentBlockWithImage