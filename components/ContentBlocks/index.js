import marked from 'marked'
import Link from 'next/link'
import SbEditable from 'storyblok-react'

const ContentBlock = ({ blok }) => {
     const body = (content) => {
          let rawMarkup = marked(content)
          return { __html: rawMarkup }
     }

     return (
          <SbEditable content={blok}>
               <div className={`content-block ${blok.modifiers ? `content-block--` + blok.modifiers : ''}`}>
                    <div className={`container`}>
                         <div className={`row row--align-center`}>
                              <div className={`col col--8`}>
                                   <div className={`content-block__content`}>
                                        <div className={`content-block__content-title`}>
                                             <h2>{blok.title}</h2>
                                        </div>
                                        {blok.content && (
                                             <div className={`content-block__content-body`} dangerouslySetInnerHTML={body(blok.content)}></div>
                                        )}
                                        <div className={`content-block__content-button`}>
                                             <Link href={blok.link.cached_url}><a className={`button`}>{blok.modifiers === 'secondary' ? 'Get Started' : 'Learn More'}</a></Link>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </SbEditable>
     )
}

export default ContentBlock