import marked from 'marked'
import Link from 'next/link'
import SbEditable from 'storyblok-react'

const ContentBlockWithQuotes = ({ blok }) => {
     const body = (content) => {
          let rawMarkup = marked(content)
          return { __html: rawMarkup }
     }

     return (
          <SbEditable content={blok}>
               <div className="content-block content-block--with-quotes">
                    <div className={`container`}>
                         <div className={`row row--align-center`}>
                              <div className={`col col--8`}>
                                   <div className={`content-block__content`}>
                                        <div className={`content-block__content-title`}>
                                             <h2>{blok.title}</h2>
                                        </div>
                                        {blok.content && (
                                             <div className={`content-block__content-body`} dangerouslySetInnerHTML={body(blok.content)}></div>
                                        )}
                                        <div className={`content-block__content-button`}>
                                             <Link href={blok.link.cached_url}><a className={`button button--primary`}>Learn More</a></Link>
                                        </div>
                                   </div>
                              </div>
                              <div className={`col col--4`}>
                                   <div className={`content-block__quote`}>
                                        {blok.quote && (
                                             <blockquote dangerouslySetInnerHTML={body(blok.quote)} />
                                        )}
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </SbEditable>
     )
}

export default ContentBlockWithQuotes