import Link from 'next/link';
import logo from '../../assets/images/logo-hii-white.svg';
import Social from '../Social';

const Footer = ({ settings }) => {
     console.log(settings);
     return (
          <footer className={`footer text--gray-300`}>
               <div className={`container`}>
                    <div className={`row row--align-center`}>
                         <div className={`col col--4`}>
                              <div className={`footer__brand-logo`}>
                                   <Link href="/"><img src={logo} /></Link>
                              </div>
                         </div>
                         <div className={`col col--4 text--xs col--justify-center text--center`}>
                              <p>&copy; 2020 Hii Digital. All Rights Reserved.</p>
                         </div>
                         <div className={`col col--4 col--justify-right`}>
                              <Social />
                         </div>
                    </div>
               </div>
          </footer>
     )
}

export default Footer