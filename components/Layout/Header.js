import Link from 'next/link';
import { useState } from 'react';
import logo from '../../assets/images/logo-hii.svg';
import Nav from './Nav';

const Header = ({ settings }) => {
     const [menuActive, setMenuState] = useState(false);
     const toggleMenu = () => { setMenuState(!menuActive) }

     return (
          <header className={`header`}>
               <div className={`container`}>
                    <div className={`row`}>
                         <div className={`col col--4`}>
                              <div className={`header__brand-logo`}>
                                   <Link href="/"><img src={logo} /></Link>
                              </div>
                         </div>
                         <div className={`col col--8 col--justify-right`}>
                              <Nav settings={settings} menuActive={menuActive} />
                              <div className={`header__nav-btn-wrapper`}>
                                   <button onClick={toggleMenu} className={`header__nav-btn ${menuActive ? 'header__nav-btn--active' : ''}`}>
                                        <div className={`hamburger ${menuActive ? 'hamburger--active' : ''}`}></div>
                                   </button>
                              </div>
                         </div>
                    </div>
               </div>
          </header>
     )
}

export default Header