import StoryblokService from '../../utils/storyblok-service';
import Hero from '../Hero';
import Footer from './Footer';
import Header from './Header';

const Layout = ({ children, home, settings }) => {

     return (
          <>
               <Header settings={settings} />
               {home ? (
                    <Hero />
               ) : (
                         <main>{children}</main>
                    )}
               <Footer />
               {StoryblokService.bridge()}
          </>
     )
}

export default Layout