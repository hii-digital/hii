import Link from 'next/link'
import React from 'react'

const Nav = ({ settings, menuActive }) => (
     <nav className={`header__nav ${menuActive ? 'header__nav--active' : ''}`}>
          <div className={`header__nav-container`}>
               <ul>
                    {settings && settings.content.main_navi.map((navitem, index) =>
                         <li key={index}>
                              <Link href={navitem.link.cached_url}>
                                   <a className="header__nav-link">{navitem.name}</a>
                              </Link>
                         </li>
                    )}
               </ul>
          </div>
     </nav>
)

export default Nav