import SbEditable from 'storyblok-react'
import Component from '../index'

const Grid = ({ blok }) => (
     <SbEditable content={blok}>
          <div className="util__flex">
               {blok.columns.map((blok, index) =>
                    <Component key={index} blok={blok} />
               )}
          </div>
     </SbEditable>
)

export default Grid