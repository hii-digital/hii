import gsap from 'gsap';
import { useEffect, useRef } from 'react';
import SbEditable from 'storyblok-react';
import arrow from '../../assets/images/arrow-gray.svg';

const Hero = ({ blok }) => {
     let hero = useRef(null);
     let flower = useRef(null);
     let content = useRef(null);
     let tagline = useRef(null);
     let tl = gsap.timeline({ defaults: { ease: 'easeIn' } })

     // gsap.registerPlugin(ScrollTrigger);
     useEffect(() => {
          gsap.to(hero, 0, { css: { visibility: 'visible', } })
          // Content
          const headlineFirst = content.children[0].children[0];
          const headlineSecond = headlineFirst.nextSibling;
          const headlineThird = headlineSecond.nextSibling;
          // Flower Animation
          tl.from(flower, 2, { y: 50 }, 'Start');
          // Content Animation
          tl.from([headlineFirst.children, headlineThird.children], { y: 100, duration: 1 }, 0)
               .from(headlineSecond.children, { y: 100, duration: 1, css: { opacity: 0 } }, 1.5, 'Start')
          // Tagline Animation
          tl.from(tagline.children, { y: 100, duration: 1 }, 0, 'Start')
     })

     return (
          <SbEditable content={blok}>
               <section className={`hero ${blok.modifiers ? `hero--` + blok.modifiers : ''}`} ref={el => hero = el}>
                    <div className={`container`}>
                         <div className={`hero__content`} ref={el => content = el}>
                              <h2>
                                   <div className={`hero__content-line hero__content-line--top`}>
                                        <div className={`hero__content-line-inner`}>
                                             {blok.above_title}
                                        </div>
                                   </div>
                                   <div className={`hero__content-line hero__content-line--main`}>
                                        <div className={`hero__content-line-inner`}>
                                             {blok.title_image && blok.title_image.filename ?
                                                  <span className={`hero__content-line-image`} style={{ backgroundImage: 'url(' + blok.title_image.filename + ')' }}>
                                                       <span className={`screen-reader-text`}>{blok.title}</span>
                                                  </span>
                                                  : blok.title}
                                        </div>
                                   </div>
                                   <div className={`hero__content-line hero__content-line--bottom`}>
                                        <div className={`hero__content-line-inner`}>
                                             {blok.below_title}
                                        </div>
                                   </div>
                              </h2>
                              {blok.tagline &&
                                   <div className={`hero__tagline`} ref={el => tagline = el}>
                                        <p>{blok.tagline}</p>
                                   </div>
                              }
                         </div>
                    </div>
                    {blok.featured_image && blok.featured_image.filename &&
                         <div className={`hero__featured-image`}>
                              <div className={`container`}>
                                   <figure>
                                        <img src={blok.featured_image.filename} alt={blok.featured_image.alt} />
                                   </figure>
                              </div>
                         </div>
                    }
                    <div className={`hero__chalkboard`}>
                         <div className={`arrow`} style={{ top: '9%', left: '16%' }}><img src={arrow} alt="" /></div>
                         <div className={`arrow`} style={{ top: '9%', left: '17.5%' }}><img src={arrow} alt="" /></div>
                         <div className={`arrow reversed`} style={{ top: '72%', left: '61%' }}><img src={arrow} alt="" /></div>
                         <div className={`arrow reversed`} style={{ top: '72%', left: '62.5%' }}><img src={arrow} alt="" /></div>
                         <div className={`arrow arrow--with-circle`} style={{ top: '42%', left: '80%' }}><img src={arrow} alt="" /></div>
                         <div className={`blob animated`} style={{ top: '2%', left: '71%' }}></div>
                         <div className={`blob filled`} style={{ top: '4%', left: '90%' }}></div>
                         <div className={`blob`} style={{ top: '11%', left: '2%', width: '4vw', height: '4vw' }}></div>
                         <div className={`blob animated`} style={{ top: '20%', left: '10%' }}></div>
                         <div className={`blob filled`} style={{ top: '55%', left: '15%', width: '1vw', height: '1vw' }}></div>
                         <div className={`blob filled`} style={{ top: '55%', left: '17%', width: '1vw', height: '1vw' }}></div>
                         <div className={`blob animated filled`} style={{ top: '91%', left: '46%' }}></div>
                         <div className={`blob`} style={{ top: '70%', left: '87%' }}></div>
                         <div className={`line`} style={{ top: '9%', left: '22%', maxWidth: '10vw' }}></div>
                         <div className={`line`} style={{ top: '91%', left: '7%', maxWidth: '10vw' }}></div>
                         <div className={`line animated`} style={{ top: '54%', left: '1%', maxWidth: '200px' }}></div>
                         <div className={`line`} style={{ top: '56%', left: '1%', maxWidth: '150px' }}></div>
                         <div className={`flower`} ref={el => flower = el}></div>
                         <div className={`rect`} style={{ top: '28%', left: '85%', width: '2vw', height: '2vw' }}></div>
                         <div className={`rect animated`} style={{ top: '66%', left: '10%', width: '5%', height: '1vw' }}></div>
                         <div className={`rect`} style={{ top: '66%', left: '16%', width: '5%', height: '1vw' }}></div>
                         <div className={`rect`} style={{ top: '66%', left: '22%', width: '5%', height: '1vw' }}></div>
                         <div className={`rect`} style={{ top: '79%', left: '50%', width: '7%', height: '0.75vw' }}></div>
                         <div className={`rect`} style={{ top: '79%', left: '58%', width: '7%', height: '0.75vw' }}></div>
                         <div className={`rect animated`} style={{ top: '79%', left: '66%', width: '7%', height: '0.75vw' }}></div>
                         <div className={`rect`} style={{ top: '90%', left: '10%', width: '6vw', height: '2vw' }}></div>
                         <div className={`square`} style={{ top: '30%', left: '86%' }}></div>
                    </div>
               </section >
          </SbEditable >
     )
}

export default Hero