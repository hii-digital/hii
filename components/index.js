import ContentBlock from './ContentBlocks'
import ContentBlockWithImage from './ContentBlocks/WithImage'
import ContentBlockWithQuotes from './ContentBlocks/WithQuote'
import Feature from './Feature'
import Grid from './Grid'
import Hero from './Hero'
import Placeholder from './Placeholder'
import ProjectsTeasers from './Projects/Teasers'
import Social from './Social'
import Teaser from './Teaser'

const Components = {
     'teaser': Teaser,
     'feature': Feature,
     'grid': Grid,
     'hero': Hero,
     'content-block': ContentBlock,
     'content-block-with-image': ContentBlockWithImage,
     'content-block-with-quote': ContentBlockWithQuotes,
     'project_teasers': ProjectsTeasers,
     'social': Social,
}

const Component = ({ blok }) => {
     if (typeof Components[blok.component] !== 'undefined') {
          const Component = Components[blok.component]
          return <Component blok={blok} />
     }
     return <Placeholder componentName={blok.component} />
}

export default Component