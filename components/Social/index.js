import { faInstagram, faLinkedinIn } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Social = ({ blok }) => {

     return (
          // <SbEditable content={blok}>
          <div className="social">
               <ul className={`list list--unstyled list--inline`}>
                    <li><a target={`_blank`} href={`https://www.linkedin.com/company/hii-digital`}><FontAwesomeIcon icon={faLinkedinIn} /></a></li>
                    <li><a target={`_blank`} href={`https://www.instagram.com/hii.digital/`}><FontAwesomeIcon icon={faInstagram} /></a></li>
               </ul>
          </div>
          // </SbEditable>
     )
}

export default Social