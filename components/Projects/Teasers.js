import { useEffect, useRef } from 'react';
import SbEditable from 'storyblok-react';


const ProjectsTeasers = ({ blok }) => {

     let teasers = useRef(null)

     useEffect(() => {
          // gsap.registerPlugin(CSSRulePlugin, ScrollTrigger);
          // gsap.to(feature, 0, {
          //      css: { visibility: 'visible', }
          // })
          console.log(teasers, this);
          // const headlineFirst = teasers.children[0].children[0].children[0]; // figure
          // const headlineSecond = headlineFirst.nextSibling;
          // const headlineThird = headlineSecond.nextSibling;
          // Feature Animation
          // tl.from(feature, {
          //      scrollTrigger: {
          //           markers: true,
          //           trigger: feature,
          //           // start: 'top center',
          //           // end: 'bottom 80%',
          //           // toggleActions: 'restart pause reverse pause',
          //      },
          //      y: 400,
          //      duration: 3
          // }, 0)
     })

     return (
          <SbEditable content={blok}>
               <div className="project-teasers" ref={el => teasers = el}>
                    <div className="project-teasers__container">
                         {blok.project_image &&
                              <div className={`project-teasers__image`}>
                                   <figure>
                                        <img src={blok.project_image.filename} alt={blok.project_image.alt} />
                                        {blok.project_image.title &&
                                             <figcaption>{blok.project_image.title}</figcaption>
                                        }
                                   </figure>
                              </div>
                         }
                         <div className={`project-teasers__title`}>
                              <p>{blok.tagline}</p>
                              <h2>{blok.title}</h2>
                         </div>
                    </div>
               </div>
          </SbEditable>
     )
}

export default ProjectsTeasers